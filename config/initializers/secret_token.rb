# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = '0d0e4c0c399468e9a597786e212521edda2a0218e9d946684a14779c09a1202895eed9a3813f3f5c643c602a81b802f0824e622604a1e9b421c3ae7948cb8a86'
